#include <Servo.h>

#define USE_USBCON
#include <ros.h>
#include <geometry_msgs/TwistStamped.h>

#define PIN_SERVO 9
#define PIN_ESC 10
#define PIN_LED 5

#define LET_LED(set) digitalWrite(PIN_LED, (set == 0 ? LOW : (set == 1 ? HIGH : HIGH-digitalRead(PIN_LED))));

ros::NodeHandle nh;

Servo steeringServo;
Servo motorESC; // The Electronic Speed Controller (ESC) works like a Servo

static const uint16_t USB_SERIAL_BAUD_RATE = 57600;
static const uint16_t WRITE_DELAY_MS = 1;

const char* engine_input_topic = "/nr/engine/input/actions";

const int diffSteer_prototyp_v1 = 20;
const int diffThrottle_prototyp_v1 = 15; // value should not be lower than 12. if lower, backwards driving does not work properly

const int diffSteer_prototyp_v2 = 35;
const int diffThrottle_prototyp_v2 = 8; // value range: 8-40 (derrived by http://forum.arduino.cc/index.php?topic=102888.0)

// General bounds for the steering servo and the ESC
const double boundary_low = -1.0;
const double boundary_high = 1.0;
const int servoNeutral = 90; // same for both prototypes
const int diffSteer = diffSteer_prototyp_v2;
const int diffThrottle = diffThrottle_prototyp_v2;
const int minSteering = servoNeutral - diffSteer;
const int maxSteering = servoNeutral + diffSteer;
const int minThrottle = servoNeutral - diffThrottle;
const int maxThrottle = servoNeutral + diffThrottle;

void blink(int _delay, int count = 1) {
  for (int i = 0; i < count; i++) {
    digitalWrite(PIN_LED, HIGH);
    delay(_delay);
    digitalWrite(PIN_LED, LOW);
    delay(_delay);
  }
}

/**
 * Arduino 'map' funtion for floating point
 */
double fmap (double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * Combined MIN and MAX.
 */
inline int between(int x, int minimum, int maximum) {
  return x < minimum ? minimum : (x > maximum ? maximum : x);
}

/**
 * Map steering input to a valid servo angle.
 *
 * @param double steer Value from boundary_low to boundary_high
 * (its the same value but could also be different from mapThrottle)
 * @return int Steering angle
 */
int mapSteering(double steer) {
  int servoAngle = (int)fmap(steer, boundary_low, boundary_high, minSteering, maxSteering);
  return between(servoAngle, minSteering, maxSteering);
}

/**
 * Map throttle input to a valid servo angle for the ESC.
 *
 * @param double throttle Value from boundary_low to boundary_high
 * (its the same value but could also be different from mapSteering)
 * @return int Throttle angle / value
 */
int mapThrottle(double throttle) {
  int servoAngle = (int)fmap(throttle, boundary_low, boundary_high, minThrottle, maxThrottle);
  return between(servoAngle, minThrottle, maxThrottle);
}

void driveCallback(const geometry_msgs::TwistStamped&  TwistStampedMsg) {
   const double steerIn = TwistStampedMsg.twist.angular.z;
   const double throttleIn = TwistStampedMsg.twist.linear.x;
   const int steer = mapSteering(steerIn);
   const int throttle = mapThrottle(throttleIn);
   steeringServo.write(steer);
   motorESC.write(throttle);
}

ros::Subscriber<geometry_msgs::TwistStamped> driveSubscriber(engine_input_topic, &driveCallback);

void setup() {
  pinMode(PIN_LED, OUTPUT);
  Serial.begin(USB_SERIAL_BAUD_RATE);

  nh.initNode();
  nh.subscribe(driveSubscriber);

  // attach the steering servo and the ESC, send neutral values
  steeringServo.attach(PIN_SERVO);
  motorESC.attach(PIN_ESC);
  steeringServo.write(servoNeutral);
  motorESC.write(servoNeutral);
  blink(500, 10);
}

void loop() {
  nh.spinOnce();
  delay(WRITE_DELAY_MS);
}
